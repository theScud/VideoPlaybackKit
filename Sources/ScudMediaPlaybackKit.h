//
//  ScudMediaPlaybackKit.h
//  ScudMediaPlaybackKit
//
//  Created by Sudeep Kini on 04/10/16.
//  Copyright © 2017 theScud. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ScudMediaPlaybackKit.
FOUNDATION_EXPORT double ScudMediaPlaybackKitVersionNumber;

//! Project version string for ScudMediaPlaybackKit.
FOUNDATION_EXPORT const unsigned char ScudMediaPlaybackKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ScudMediaPlaybackKit/PublicHeader.h>
