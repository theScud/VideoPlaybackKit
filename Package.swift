//
//  ScudMediaPlaybackKit.swift
//  ScudMediaPlaybackKit
//
//  Created by Sudeep Kini on 23/10/15.
//  Copyright © 2017 theScud. All rights reserved.
//

import PackageDescription

let package = Package(
    name: "ScudMediaPlaybackKit",
    dependencies: [],
    exclude: ["Tests"]
)
