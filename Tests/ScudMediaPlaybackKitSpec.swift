//
//  ScudMediaPlaybackKitSpec.swift
//  ScudMediaPlaybackKit
//
//  Created by Sudeep Kini on 04/10/16.
//  Copyright © 2017 theScud. All rights reserved.
//

import Quick
import Nimble
@testable import ScudMediaPlaybackKit

class ScudMediaPlaybackKitSpec: QuickSpec {

    override func spec() {

        describe("ScudMediaPlaybackKitSpec") {
            it("works") {
                expect(ScudMediaPlaybackKit.name) == "ScudMediaPlaybackKit"
            }
        }

    }

}
